        var jqCronInst = {} 
        function generateIds(){
            var table = document.getElementById("cronTable");
            for(var i = 1; i < table.rows.length; i++) {
                var cells = table.rows[i].getElementsByTagName('td');
                cells[1].children[0].id = "cronJob" + i;
                }
        }       
        function addCronJob(value, command) {
            generateIds();
            var table = document.getElementById("cronTable");
            var row = table.insertRow(table.rows.length);
            var cell1 = row.insertCell(0);
            cell1.innerHTML = '<input type="checkbox" name="select">';
            var cell2 = row.insertCell(1);
            var divId = "cronJob";
            divId += table.rows.length - 1;
            cell2.innerHTML = '<div id=' + divId + '></div>';
            createJqCron('#' + divId, value);
            var cell3 = row.insertCell(2); 
            cell3.innerHTML = '<input type="checkbox" name="little">';
            var cell4 = row.insertCell(3);
            cell4.innerHTML = '<select id="delay1_' + divId + '"></select>'; 
            var cell5 = row.insertCell(4); 
            cell5.innerHTML = '<input type="checkbox" name="big" checked>';
            var cell6 = row.insertCell(5);
            cell6.innerHTML = '<select id="delay2_' + divId + '"></select>';
            var select = '';
    		for (i=10;i<=200;i+=10){
    					select += '<option val=' + i + '>' + i + '</option>';
				}
			$('#' + 'delay1_' + divId).html(select);
			$('#' + 'delay2_' + divId).html(select);
            var delay = command.split(' ').pop();
            var arr = command.split(' ');
            arr.pop();
            command = arr.join(' ');
            //console.log("command:"+command+"delay:"+delay);
                switch(command) {
                    case '/usr/bin/little_bell':
                        cell3.children[0].checked = true;
                        cell5.children[0].checked = false;
                        $('#delay1_' + divId).val(delay);                                            
                    break;
                    case '/usr/bin/big_bell':
                        cell5.children[0].checked = true;
                        cell3.children[0].checked = false;
                        $('#delay2_' + divId).val(delay);
                    break;
                    case '/usr/bin/bells':
                        cell3.children[0].checked = true;
                        cell5.children[0].checked = true;
                        $('#delay1_' + divId).val(delay);
                        $('#delay2_' + divId).val(delay);
                    break;                    
                }
        }
        function removeCronJobs() {
            var table = document.getElementById("cronTable");
            var nrRows = table.rows.length;
            for(var i = 1; i < nrRows; i++) {
                var cells = table.rows[i].getElementsByTagName('td');
                if(cells[0].children[0].checked){
                    delete jqCronInst[ "cronJob" + i]; 
                    table.deleteRow(i);
                    removeCronJobs();//recursive call
                    break;
                }
            }
        }
        var createJqCron = function(id, cronValue){
        jqCronInst[id] = ($(id).jqCron({
        enabled_minute: true,
        multiple_dom: true,
        multiple_month: true,
        multiple_mins: true,
        multiple_dow: true,
        multiple_time_hours: true,
        multiple_time_minutes: true,
        default_period: 'week',
        default_value: cronValue,
        no_reset_button: false,
        lang: 'en'
        }).jqCronGetInstance()); 
        };
        var readSystemCron = function(id){
            var table = document.getElementById("userCron");
            for(var i = 0; i < table.rows.length; i++) {
                var cronText = table.rows[i].children[0].innerText;
                var arr = cronText.split(' ');
                var delta = arr.pop();
                var command = arr.pop();
                command += ' ' + delta;                
                //console.log("command:" + command);
                //console.log(arr.join(' '));                
                addCronJob(arr.join(' '),command);
            }
        }
        
        $(function(){
            readSystemCron();
        });
        function saveCronJobs() {
            console.log(jqCronInst);
            var table = document.getElementById("cronTable");
            var myTableArray = [];
            for(var i = 1; i < table.rows.length; i++) {
                var cells = table.rows[i].getElementsByTagName('td');
                    console.log(jqCronInst["#cronJob" + i].getCron());
                var command = null;
                if(table.rows[i].cells[2].children[0].checked && table.rows[i].cells[4].children[0].checked){
                    command = "/usr/bin/bells ";
                    command += $('select#delay1_cronJob' + i).val();
                    //console.log(command);
                }else if(table.rows[i].cells[2].children[0].checked){
                    command = "/usr/bin/little_bell ";
                    command += $('select#delay1_cronJob' + i).val();
                    //console.log($('select#delay1_cronJob' + i).val());                    
                }else if(table.rows[i].cells[4].children[0].checked){
                    command = "/usr/bin/big_bell ";
                    command += $('select#delay2_cronJob' + i).val();
                    //console.log($('select#delay2_cronJob' + i).val());
                }
                else {
                    alert("Invalid!");
                }
                if(command != null){
                    myTableArray.push(jqCronInst["#cronJob" + i].getCron() + ' ' + command);
                }
            }
            var data_to_send ={}
            data_to_send["data"] = JSON.stringify(myTableArray);
            $.ajax({
                data: data_to_send,
                url: 'crontab.php',
                method: 'POST', // or GET
                success: function() {alert('Sikeres mentés!');
                         window.location.reload();//reload page
                        }                  
            });
        }
        