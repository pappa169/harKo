from crontab import CronTab

class Schedule:
    def __init__(self, name):
        self.cron = CronTab(user=name)
        job = self.cron.new(command='/usr/bin/echo')
        job.day.on(4, 5, 6)
        job.enable()
        self.cron.write()
        for job in self.cron:
            print job

if __name__ == "__main__":
    sched = Schedule('pappa')
