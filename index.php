<!DOCTYPE html>
<html>
    <head>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
        <title>Harangozó</title>
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <!--<link rel="stylesheet" href="style.css"-->
        <script src="//code.jquery.com/jquery-1.10.1.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="jqCron/jqCron.js"></script>
        <script src="jqCron/jqCron.en.js"></script>
        <script src="harKo.js"></script>
        <link rel="stylesheet" href="jqCron/jqCron.css">
        <link rel="stylesheet" href="harKo.css">
    </head>
    <header>    
    <h2>
    <?php
        echo "Jelen idő: " . '<span class="date">' . date("H:i") . '</span>, dátum: ' . '<span class="date">' . date("Y-m-d, D") . '</span>';
    ?>
    </h2>
    </header>
    <body>
        <section id="cronEditor" style="display: inline;">
        <br/>
        <table id="cronTable">
          <tr>
            <th></th>
            <th>Mikor?</th>
            <th>Kis<br/>Harang</th>
            <th></th>
            <th>Nagy<br/>Harang</th>
            <th></th>
          </tr>
        </table>
        <br/>
        <button onclick="addCronJob()">Új</button>
        <button onclick="removeCronJobs()">Töröl</button>
        <br/><br/>
        <button onclick="saveCronJobs()">Mentés</button>
        </section>
    </body>
    <footer>
    <p>Készítette Sánta Árpád.(<a href="mailto:pappa@ppabox.net">pappa@ppabox.net</a>)</p>
    </footer>    
    <div id="actualCron" style="display:none;">
        <?php
            //write the system cron to a DOM table    
			$output = shell_exec('crontab -l');
			$lines = explode(PHP_EOL, $output);
			echo '<table id="userCron">';
			foreach($lines as $line){
				if(strlen($line))
					echo '<tr><td>'. $line . '</td></tr>';
			}
			?>
</html>